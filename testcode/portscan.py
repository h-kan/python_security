#!/usr/bin/env python
#new feature
import socket
import subprocess
import sys
from datetime import datetime

# Clear the screen
subprocess.call('clear', shell=True)

# Ask for input
remoteServer	= raw_input("Enter remote host to scan: ")
remoteServerIP	= socket.gethostbyname(remoteServer)

# Print a nice banner with information on which host we are about to scan
print "-" * 60
print "Please wait, scanning remote host", remoteServerIP
print "-" * 60

# Check what time scan started
t1 = datetime.now()

# Using the range function to specify ports (here it will scan all port between 1 and 1024)
# We also put in some error handling for catching errors

try:
    for port in range(1,1025):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	result = sock.connect_ex((remoteServerIP, port))
	if result == 0:
	    print "Port {}: \t Open".format(port)
	sock.close()

except KeyboardInterrupt:
    print "You pressed Ctrl+C"
    sys.exit()

except socket.gaierror:
    print "Hostname could not be resolved"
    sys.exit()

except socket.error:
    print "Couldn't conenct to server"
    sys.exit()

# Cehcking time again
t2 = datetime.now()

# Calculate diff of time,to se how long it took to run scanning
total = t2 - t1

# Print info to screen
print "Scanning completed in: ", total
