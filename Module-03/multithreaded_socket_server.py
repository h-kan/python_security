#!/usr/bin/env python
#
# A multithreading echo server using BSD socket module
# and threading module.

# Check that all modules exists
try:
    import sys
    import os
    import socket
    import threading

except ImportError, err:
    print "%s Failed: %s" % (__file__,err)
    sys.exit(1)


### Variables and Lists ###
HOST = '0.0.0.0'
PORT = 8080
threads = []
###########################

class ClientHandler(threading.Thread):
    ''' Create one instance for every connection to the server and
    keep track of ip and port from client in each tread '''

    # Constructor, this part will be called when a new object(newthread) gets
    # created
    def __init__(self,ip, port, socket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket
        print "<*> Creating new thread for "+self.ip+":"+str(self.port)

    # This part will run when the created object(newthread) calls with start
    def run(self):
        print "Connection from : "+self.ip+":"+str(self.port)
        self.socket.send("\nConnected to Threading Server\n")
        # Make sure data is something
        data = "dummy"
        # Run as long as data is something
        while len(data):
            data = self.socket.recv(2084)
            print "Got data : "+data+"from client: "+self.ip+":"+str(self.port)+"\n"
            self.socket.send("Server echos: "+data)
        # If recived data is nothing, quit the loop
        print "Client disconnected : "+self.ip+":"+str(self.port)


if __name__ == '__main__':
    try:
        # Create socket with proto INET with type STREAM
        tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Reuse socket in TIME_WAIT state, without waiting for its natural
        # timeout to expire.
        tcpsock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
        # Bind the socket to interface and port
        tcpsock.bind((HOST,PORT))

        while True:
            # backlog, maximum number of queued connections
            tcpsock.listen(4)
            # clear screen
            os.system('clear')#
            print "\nStaring ECHO server..."
            print "\nListening for new connections"
            # tcpsock.accept() returns conn and address and we store these
            # values in clientsock
            (clientsock, (ip, port)) = tcpsock.accept()
            # Create new object by call feeding the constructor with clientsock
            # ip and port of the connected client
            newthread = ClientHandler(ip, port, clientsock)
            # Start the new onject
            newthread.start()
            # Add the new object to list of all threads
            threads.append(newthread)

        # Wait for all threads to finsih
        for t in threads:
            t.join()

    # Catch KeyboardInterrupt and quit program
    except KeyboardInterrupt:
        print "\nYou pressed Ctrl+C"
        sys.exit()

