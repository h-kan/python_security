#!/usr/bin/python

import getopt
import signal
import sys
from scapy.all import *

def usage():
    print "USAGE: -h = help(display this message)"
    print "       -s = sniff http packet info"
    #print sys.argv[0] + " -hs"
    sys.exit(2)

def ctrl_handler(signum, frm):
    print "[ERROR!] CTRL+c: Stopping Sniffer"
    sys.exit(0)

print "[INFO] Ctrl+c to exit"
signal.signal(signal.SIGINT, ctrl_handler)

try:
    opts, args = getopt.getopt(sys.argv[1:], "hs")
    if len(opts) == 0:
        usage()
    else:
        print "\n\n\n[INFO] Creating Sniffer"
        print "[INFO] Starting dniffer"
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                usage()
            elif opt in ("-s", "--sniff"):
                print "[INFO] HTTP Packet Option Selected"
                while True:
                    pkts = sniff(iface="eth0", filter="tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) !=0)", prn=lambda x: x.getlayer(Raw))

except getopt.GetoptError,err:
    print "[!ERROR]" + str(err)
    usage()
