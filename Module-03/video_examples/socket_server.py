#!/usr/bin/python

import socket
import threading
import time

tcpSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpSocket.bind(("0.0.0.0", 8081))
tcpSocket.listen(2)
#queue = Queue.Queue()

class WorkerThread(threading.Thread) :

    def __init__(self, threadID, name, counter) :
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter

    def run(self) :
        print "Starting " + self.name
        print "Waiting for client ..."
        (client, ( ip, sock)) = tcpSocket.accept()

        print "Received connection from : ", ip

        print "Starting ECHO output ... "

        data = 'dummy'

        while len(data) :
            data = client.recv(2048)
            print "Client sent: ", data 
            client.send(data)

        print "Closing connection ..."
        client.close()

max_thread=5
i = 1
while i < max_thread:
    counter=1
    print "Creating worker thread %d" %i
    worker=WorkerThread(counter, "Thread"+str(counter), counter)
    worker.start()
    print "Worker thread %d created" %i
    counter +=1
    i +=1
    print counter
    print i
