#!/usr/bin/python
#
# Threading Socketserver that listen to port and print all traffic on it

import SocketServer
import threading

class EchoHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        print "Got Connection from : ", self.client_address
        data = 'dummy'

        while len(data):
            data = self.request.recv(1024)
            print "Client sent: " + data
            self.request.send(data)

        print "Client left"

class WorkerThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        server.serve_forever()


serverAddr = ("0.0.0.0", 9000)
server = SocketServer.TCPServer(serverAddr, EchoHandler)

for i in range(1, 3):
    print "Creating worker thread %d" %i                                                            
    worker=WorkerThread()                                
    worker.start()                                                                                  
    print "Worker thread %d created" %i
