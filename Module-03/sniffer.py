#!/usr/bin/python
#
# Very basic packetsniffer that inspects the whole TCP/IP packet and prints the output if src/dst port is 80/HTTP traffic


import socket
import struct
import binascii
import sys

# Function that inserts ":" in the MAC address
def macfix(addr):
    result = "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x" % (ord(addr[0]), ord(addr[1]), ord(addr[2]), ord(addr[3]), ord(addr[4]), ord(addr[5]))
    return result


# Main loop that starts gathering packets
try:
        while True:
        
            rawSocket = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.htons(0x0800))

            pkt = rawSocket.recvfrom(2048)
            
            # Split out headers
            ethernetHeader = pkt[0][0:14] # Store the first 14 bytes 
            ipHeader = pkt[0][14:34] # Store the next 20 bytes
            tcpHeader = pkt[0][34:54] # Store the next 20 bytes
            rawData = pkt[0][55:] # Store the rest of the package
            data = binascii.b2a_qp(rawData,quotetabs=False,istext=True)

            # Unpack the ethernet header
            eth_hdr = struct.unpack("!6s6s2s", ethernetHeader) # Get the soure & dest MAC address
            src_eth = eth_hdr[0]
            dst_eth = eth_hdr[1]


            # Unpack IP header
            ip_hdr = struct.unpack("!1s1s2s4s1s1s2s4s4s", ipHeader) # Get all the importet fields in the IP header
            version = binascii.hexlify(ip_hdr[0])
            typ_serv = binascii.hexlify(ip_hdr[1])
            length = binascii.hexlify(ip_hdr[2])
            ttl = binascii.hexlify(ip_hdr[4])
            ttl = int(ttl, 16)
            protocol = binascii.hexlify(ip_hdr[5])
            hdr_chksum = binascii.hexlify(ip_hdr[6])
            src_ip = socket.inet_ntoa(ip_hdr[7])
            dst_ip = socket.inet_ntoa(ip_hdr[8])

            # Unpack TCP header
            tcp_hdr = struct.unpack("!2s2s4s4s8s", tcpHeader) # Get all information in the TCP Header
            src_port = binascii.hexlify(tcp_hdr[0])
            src_port = int(src_port, 16)
            dst_port = binascii.hexlify(tcp_hdr[1])
            dst_port = int(dst_port, 16)
            seq_num = binascii.hexlify(tcp_hdr[2])
            seq_num = int(seq_num, 16)
            ack_num = binascii.hexlify(tcp_hdr[3])
            ack_num = int(ack_num, 16)
            
            # Check for HTTP traffic and skip all other output from getting printed
            if src_port == 11211 or dst_port == 11211:
                # Print output
                print "[+] ETHERNET"
                print "[+] Src MAC: %s" %str(macfix(src_eth))
                print "[+] Dst MAC: %s" %str(macfix(dst_eth))
                print "[+]"
                print "[+] IP"
                print "[+]  Src IP: " + src_ip
                print "[+]  Dst IP: " + dst_ip
                print "[+]  IP Version: " + version
                print "[+]  Protocol: " + protocol
                print "[+]  Type of Service: " + typ_serv
                print "[+]  Total Length: " + length
                print "[+]  Checksum: " + hdr_chksum 
                print "[+]  TTL: %s " %ttl
                print "[+]"
                print "[+] TCP"
                print "[+]      TCP Src Port: %s" %src_port
                print "[+]      TCP Dst Port: %s" %dst_port
                print "[+]      TCP Seq Num: %s" %seq_num 
                print "[+]      TCP Ack Num: %s" %ack_num
                print "[+]"
                print "[+] DATA : \n %s" %data
                print "======================================"

except KeyboardInterrupt:
    print "\nYou pressed Ctrl +C"
    sys.exit()

