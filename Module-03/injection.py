#!/usr/bin/python


import socket
import struct
import sys
import binascii


def createEthPkt(dstMac,srcMac,proto):
    return struct.pack("!6s6s2s", dstMac, srcMac, proto)

def creteArpPkt(srcMac,srcIp,dstMac,dstIp):
    return struct.pack("!6s4s6s4s", srcMac, socket.inet_aton(srcIp), dstMac, socket.inet_aton(dstIp))
    
def createFinalPkt(packet):
    while len(packet) < 46:
        packet += struct.pack("B",0x00)
    return packet

def sendPkt(finalPkt):
    pkts = 0
    print "[+] Sending ARP Request..."
    while pkts <= 100:
        pkts=pkts+1
        print "[+] Sent %s ARP Request"%pkts
        rawSocket.send(finalPkt)
    print "[+] Done sending "

rawSocket = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.htons(0x0800))
rawSocket.bind(("eth0", socket.htons(0x0800)))


proto = '\x08\x06'
srcMac = '\x00\x00\x00\x00\x00\x00'
dstMac = '\x11\x11\x11\x11\x11\x11'
srcIp = '192.168.0.1'
dstIp = '192.168.0.20'

ethPkt  = createEthPkt(dstMac,srcMac,proto)
arpPkt  = ethPkt + creteArpPkt(srcMac,srcIp,dstMac,dstIp)
finalPkt = createFinalPkt(arpPkt)

sendPkt(finalPkt)
