

import socket
import os

tcpSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

tcpSocket.bind(("0.0.0.0", 8000))

tcpSocket.listen(2)

def child_connection (client, ip, sock) :
    print "Child Process"
    print "Waiting for client ... "
    #(client, ( ip, sock))

    print "Received connection from : ", ip

    print "Starting echo output ... "

    data = 'dummy'

    while len(data) : 
        data = client.recv(2048)
        print "Client :", ip,"sent: ",data
        client.send(data)

    print "Closing connection ... "
    client.close()
    
def parent_process() :
    print "Parent Process"
    childpid = os.fork()
    if childpid == 0 :
        (client, ( ip, sock)) = tcpSocket.accept()
        child_connection(client, ip, sock)
    else :
        print "inside parent"

    while True:
        pass 

parent_process()
