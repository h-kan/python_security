#!/usr/bin/python

import os

def child_process () :
    print " I am the child process and my pid is : %d" %os.getpid()

    print " The child exiting"

def parent_process() :
    print " I am the parent process with pid : %d" %os.getpid()

    childpid = os.fork()

    if childpid == 0 :
        # we are inside the child_process
        child_process()

    else : 
        # we are inside parent process
        print "We are inside the parent process"
        print "Our child has the PID : %d" %childpid

    while True :
        pass

parent_process()
